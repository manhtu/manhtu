var objectData = null;
var objectData2 = null;
var name = null;
$(function(){

	/***************************************
				Time Line Init
	***************************************/
	if(typeof VMM != 'undefined'){
	var timeline = new VMM.Timeline();
    
	    timeline.init("data.json");
	    $.ajax({
	      dataType: "json",
	      url: 'fulldata.json',
	      success: function(results){
	        objectData = results['history'];
	      }
	    });
	    $.ajax({
	      dataType: "json",
	      url: 'data.json',
	      success: function(results){
	        objectData2 = results['timeline']['date'];
	      }
	    });
	}
	/***************************************
				Menu 
	***************************************/
	createNav('header');
	createMenu('LIÊN HỆ','#','contact');
	createMenu('ĐĂNG NHẬP','#','signin');
	createMenu('ĐĂNG KÝ','#','register');
	createMenu('THÔNG TIN','#','about');
	createMenu('SỰ KIỆN',realPath(),'events','float:left;margin-left:30px;');
	createMenu('ANH HÙNG',realPath()+'plugin/HeroesList/demo.html','heroes','float:left;');
	createMenu('TRÒ CHƠI',realPath()+'plugin/Games','games','float:left;');

	$('.header_user_info').each(function(){
   		$(this).attr('name','');
   	});
   	$('#events').attr('name','active');

	/***************************************
				Sigin & Register
	***************************************/

	$('#signin').click(function(){
		_createPopup();
		$('#popup').append('<div class="title">Login</div>\
							<form id="loginForm" name="loginForm" method="POST" action="">\
								<p class="message">Fill out the form below to login to Jewelry Site</p>\
								<input class="loginElement" type="text" name="loginName" value="" placeholder="User name" />\
								<input class="loginElement" type="password" name="loginPass" value="" placeholder="Password" />\
								<a class="forgotPass" href="#">Forgot your password ?</a>\
								<a class="crateAccount" href="#">Create a free account</a>\
								<a href="#" class="button2">Ok, Let me in! <i class="icon-chevron-right">></i></a>\
							</form>');
		$('#popup').fadeIn(1000);
	});
	$('#register').click(function(){
		_createPopup();
		$('#popup').append('<div class="title">Register</div>\
							<form id="loginForm" name="registerForm" method="POST" action="">\
								<p class="message">Fill out the form below to register to Jewelry Site</p>\
								<input class="loginElement" type="text" name="loginName" value="" placeholder="User name" />\
								<input class="loginElement" type="password" name="loginPass" value="" placeholder="Password" />\
								<input class="loginElement" type="text" name="loginEmail" value="" placeholder="Email" />\
								<a class="alreadyAccount" href="#">Already account? Login here!</a>\
								<a href="#" class="button2">Sign up me NOW! <i class="icon-chevron-right">></i></a>\
							</form>');
		$('#popup').fadeIn(1000);
	});

	/***************************************
				Search Function
	***************************************/
	createSearch('#','POST',realPath()+"assets/img/sbicon.png");
	$('#search_query_top').keyup(function(){
		actionSearch();
	});
	$("#searchbox button").click(function(){
		actionSearch();
	});
	$("#searchbox button img").click(function(){
		actionSearch();
	});
	$('.content').click(function(){
			
	});
	$('html').click(function (e) {
    if ($(e.target).hasClass("flag") || $(e.target).parent().hasClass("searchResultElement") || $(e.target).hasClass("search_button")) {
    }else{
    	$('#searchResultList').html('');
    }
});

});
function actionSearch(){
	$('#searchResultList').html('');
	var keyword = $('#search_query_top').val();
	/***********************************
			Search From Events
	***********************************/
	count = 0;
	if(keyword != ''){
		if($('#events').attr('name')=='active'){
			for(var i=0;i<objectData2.length;i++){
				if(compareElement(keyword,objectData2[i]['headline']) || compareElement(keyword,objectData2[i]['startDate'])){
					pushElementSearchResult(objectData2[i],i);
					count++;
				}
			}
			if(count == 0)
				$('#searchResultList').append('<div class="searchResultElement"><p>Không tìm thấy dữ liệu phù hợp</p></div>');
		}
	/***********************************
			Search From Heroes
	***********************************/	
		else if($('#heroes').attr('name')=='active'){
			var i=0;
			$('#list').children("li").each(function(){
				if(compareElement(keyword,$(this).attr('value'))){
					pushElementSearchHero($(this),i);
					count++;
				}
				i++;
			});
			if(count == 0)
				$('#searchResultList').append('<div class="searchResultElement"><p>Không tìm thấy dữ liệu phù hợp</p></div>');
		}
	}
	$('.searchResultElement').click(function(){
		if($('#heroes').attr('name')=='active'){
		 	$("ul").roundabout("animateToChild", $(this).attr("name"));
		}else{
			$('"[id=\''+$(this).attr('name')+'\']"').children('.flag').trigger("click");
		}

	});
	
}
function pushElementSearchResult(obj,i){
	var content = '<div class="searchResultElement" name="'+obj['startDate']+'-'+i+'"><p>'+obj['headline']+' - '+obj['startDate'].replace(',',' / ')+'</p></div>';
	$('#searchResultList').append(content);
}
function pushElementSearchHero(obj,i){
	var content = '<div class="searchResultElement" name="'+i+'"><p>'+obj.attr('value')+'</p></div>';
	$('#searchResultList').append(content);	
}
function compareElement(e1,e2){
	e1 = e1.toLowerCase();
	e2 = e2.toLowerCase();
	if(e2.indexOf(e1)== -1 ) return false;
	else if(e2.indexOf(e1) >-1 ) return true;
}
function realPath(){
	
    var rootPath = window.location.protocol + "//" + window.location.host + "/";
    if (window.location.hostname == "localhost" || window.location.hostname == "yehn.dev")
    {
        var path = window.location.pathname;
        if (path.indexOf("/") == 0)
        {
            path = path.substring(1);
        }
        path = path.split("/", 1);
        if (path != "")
        {
            rootPath = rootPath + path + "/";
        }
    }
    return rootPath;
}
function createSearch(action,method,link){
	var content = '<div id="searchbox"> \
		<input type="text" value="" placeholder="Search" name="search_query" id="search_query_top" autocomplete="off"> \
		<button class="btn btn-default button-search search_button" name="submit_search" type="submit"><img class="search_button" src="'+link+'" />\
		</button> \
		<div id="searchResultList"></div> \
	</div>';
	$('body').append(content);
}
function actionClick(){
	$(".media").click(function(){
		var img = $(this).children('.media-wrapper').children('.media-container').children('img').attr('src');
		var title = $(this).parent('.content-container').children('.text').children('.container').children('h3').html();
		var id = $(this).children('.media-wrapper').children('.media-container').children('.caption').html();
		_createPopup();
		$('#popup').append('<div class="title">'+title+'</div>');
		$('#popup').append('<div class="menuLeft"></div>');
			$('.menuLeft').append('<div class="overview _active"><img src="assets/img/overview.png"/> <div class="labelIcon">Tổng quan</div> </div>');
			$('.menuLeft').append('<div class="photo"><img src="assets/img/icon_album.png"/><div class="labelIcon">Hình ảnh</div></div>');
			$('.menuLeft').append('<div class="video"><img src="assets/img/Video-icon.png"/><div class="labelIcon">Video</div></div>');
			$('.menuLeft').append('<div class="document"><img src="assets/img/document.png"/><div class="labelIcon">Tài liệu</div></div>');
		$('#popup').append('<div class="contentMid"></div>');
		$('#popup').append('<div class="comment"></div>');
		$('.comment').append('<div class="commentTitle" name="2">Comment</div>');
		$('.comment').append('<div class="commentList"></div>');
		$('.comment').append('<div class="userComment"></div>');
		if(name != null)
			$('.userComment').append('<input class="commentName" placeholder="your name" disabled value="'+name+'"/> ');
		else
			$('.userComment').append('<input class="commentName" placeholder="your name" /> ');
		$('.userComment').append('<input class="commentContent" placeholder="your comment"/> ');
		currentObj = 0;
		currentCmt = 0;
		$('.commentTitle').click(function(){
			if($('.commentTitle').attr("name") == "2"){
				showComment();
				$(this).attr("name","1");
			}
			else{
				hideComment();
				$(this).attr("name","2");
			}
		});
		for(var i=0;i<objectData.length;i++){
			if(objectData[i]['id'] == id){
				currentObj = i;
				for(var j=0;j<objectData[i]['comment'].length;j++){
					$('.comment .commentList').append("<div><span class='userCommentName'>"+objectData[i]['comment'][j]['name']+"</span> \
					<span class='userCommentContent'>"+objectData[i]['comment'][j]['comment']+"</span></div>");
				}
			}
		}
		currentCmt = objectData[currentObj]['comment'].length;
		$( ".commentContent" ).keypress(function( event ) {
			if ( event.which == 13 ) {
				if($('.commentName').val() && $(this).val()){
					$('.comment .commentList').append("<div><span class='userCommentName'>"+$('.commentName').val()+"</span> \
					<span class='userCommentContent'>"+$(this).val()+"</span></div>");
					objectData[currentObj]['comment'][currentCmt] = new Array('name','comment');
					objectData[currentObj]['comment'][currentCmt]['name'] = $('.commentName').val();					
					objectData[currentObj]['comment'][currentCmt]['comment'] = $(this).val();
					$(this).val('');
					name = $('.commentName').val();
					$('.commentName').attr("disabled","true");
				}
			}
		});
		overview(id);
		$('.photo').click(function(){
			picture(id);
		});
		$('.overview').click(function(){
			overview(id);
		});
		$('.video').click(function(){
			video(id);
		});
		$('.document').click(function(){
			_document(id);
		});
		$('.menuLeft div').click(function(){
			$('.menuLeft div').each(function(){
				$(this).removeClass('_active');
			})
			$(this).addClass('_active');	
		});
		
	});
}
function toggleComment(){
	if($('.commentTitle').attr("name") == "2"){
		$('.commentList').css('display','none');
		$('.userComment').css('display','none');
		$('.comment').css('height','30px');
		if($('.contentMid').attr("name")=="overview"){
			$('.contentMid').css({width:"89%",left:"9%"});
		}
		else $('.contentMid').css({left:"22%"});
	}
	else{
		$('.commentList').css('display','block');
		$('.userComment').css('display','block');
		$('.comment').css({height:"90%"});
		if($('.contentMid').attr("name")=="overview")
			$('.contentMid').css({width:"62%",left:"9%"});
		else {
			$('.contentMid').css({left:"9%"});
		}
	}
		
}
function hideComment(){
	$('.commentList').hide();
	$('.userComment').hide();
	$('.comment').animate({height:"30px"});
	if($('.contentMid').attr("name")=="overview")
		$('.contentMid').animate({width:"89%"});
	else $('.contentMid').animate({left:"22%"});
}
function showComment(){
	$('.commentList').show();
	$('.userComment').show();
	$('.comment').animate({height:"90%"});
	if($('.contentMid').attr("name")=="overview")
		$('.contentMid').animate({width:"62%"});
	else {
		$('.contentMid').animate({left:"9%"});
	}
	
}
function _document(id){
	for(var i=0;i<objectData.length;i++){
			if(objectData[i]['id'] == id){
				for(var j=0;j<objectData[i]['document'].length;j++){
					var url = objectData[i]['document'][j]['url'];
					var ext = url.split('.').pop();
					filename = url.substring(url.lastIndexOf('/')+1);
					if(ext == 'doc' || ext == 'docx'){
						var content = '<a href="'+objectData[i]['document'][j]['url']+'">'+ '<div><img class="icon_document" src="'+realPath()+'assets/img/Word_Doc_Icon.png"/> <span>'+filename+'</span>' +'</a>';
					}else if(ext == 'rar' || ext == 'zip' || ext == 'gz' || ext == 'tar'){
						var content = '<a href="'+objectData[i]['document'][j]['url']+'">'+ '<div><img class="icon_document" src="'+realPath()+'assets/img/compressed.png"/> <span>'+filename+'</span>' +'</a>';
					}else{
						var parser = document.createElement('a');
							parser.href = url;
						var content = '<a href="'+objectData[i]['document'][j]['url']+'">'+ '<div><img class="icon_document" src="'+realPath()+'assets/img/goto.jpg"/> <span>'+parser.hostname;+'</span>' +'</a>';
					}
					$('#popup .contentMid').html(content);
				}
			}
		}
}
function overview(id){
	for(var i=0;i<objectData.length;i++){
			if(objectData[i]['id'] == id){
				$('#popup .contentMid').html("<div class='overview'>"+setFormatText(objectData[i]['content'])+"</div>");
			}
		}
	$('.contentMid').attr("name","overview");
	toggleComment();
}
function setFormatText(text){
	 text = text.replace(/endline/g,"<br />");
	 return text;
}
function picture(id){
	$('.contentMid').css("width","62%");
	var content = '<div id="gallery" style="width:680px;"><div id="slides">';
	for(var i=0;i<objectData.length;i++){
		if(objectData[i]['id'] == id){
			for(var j=0;j<objectData[i]['img'].length;j++){
				content += '<div class="slide"><img src="'+objectData[i]['img'][j]['url']+'" width="'+$('.contentMid').width()+'" height="'+($('.menuLeft').height()-40)+'" alt="side" /></div>';
			}
		}
	}

    content+= '</div>\
    <div id="menu">\
    <ul>\
        <li class="fbar">&nbsp;</li>';
    for(var i=0;i<objectData.length;i++){
		if(objectData[i]['id'] == id){
			for(var j=0;j<objectData[i]['img'].length;j++){
        		content += '<li class="menuItem"><a ><img src="'+objectData[i]['img'][j]['url']+'" alt="thumbnail" /></a></li>';
        	}
        }
    }
    content += '</ul>\
    </div></div>';
    $('#popup .contentMid').html(content);
    $('#slides').css('height',$('.menuLeft').height()-40);
    startSlide();
    $('.contentMid').attr("name","photo");
    toggleComment();
}
function video(id){
	$('.contentMid').css("width","62%");
	var content = '<ul class="bxslider">';
	for(var i=0;i<objectData.length;i++){
		if(objectData[i]['id'] == id){
			for(var j=0;j<objectData[i]['video'].length;j++){
				content += '<li><iframe src="'+convertVideo(objectData[i]['video'][j]['url'])+'" width="'+$('.contentMid').width()+'" height="'+($('.menuLeft').height()-40)+'" alt="side" webkitAllowFullScreen mozallowfullscreen allowFullScreen ></iframe></li>';
			}
		}
	}

   
    content += '</ul>';
    $('#popup .contentMid').html(content);
    $('.bxslider').bxSlider({
		useCSS: false,
		hideControlOnEnd: true,
		pager: false,
		controls: true,
		video:true,
		buildPager: function(slideIndex){

		}
	}); 
	$(".bx-viewport").css("height","auto");
	$('.contentMid').attr("name","video");
	toggleComment();
}
function convertVideo(url){
	if(url.search("youtube") > 1){
		if(url.search('watch') >1 )
			var url = url.replace("https://","//");
			var url = url.replace("http://","//");
			var url = url.replace("watch","embed/");
			var url = url.replace("?v=","");
			return url
	}
	return url;
}
function _createPopup(){
	$('body').append('<div id="containPopup"></div>\
							<div id="popup">\
							</div>');
	$('#containPopup').click(function(){
		$(this).remove();
		$("#popup").remove();
	});
}
function createNav(element){
	$(element).prepend('<div class="nav" >\
								<div class="container" >\
									<div class="row">\
										<nav>\
										</nav>\
									</div>\
								</div>\
							</div>');
}
function createMenu(name,link,id,style){
	var content = 	'<div class="header_user_info" style="'+style+'">\
						<a class="item" id="'+id+'" href="'+link+'"> '+name+' </a> \
					</div>'	;
	$('nav').append(content);
}
